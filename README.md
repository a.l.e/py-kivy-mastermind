# Mastermind

A mastermind in Python that can be played on Android.

## The master plan

The goal:

- The computer picks 5 colors out of a set (colors can be repeated).
- The player has to guess which color is at each position.
- The computer gives hints, how many guesses are correct and how many color are guessed correctly, but the position is wrong.

The tasks:

- An engine with the logic (must be testable)
  - Let the computer generate a set of colors
  - Check each item in the attempt if it's the same as in the solution
  - How many times the same color is in the attempt and compare with the same count in the solution.
- A GUI that allows the interactions
  - User composes an attempt
  - Show how many color are at the correct positon and how many are correct but not at the correct position.

The plan:

- Check each item in the attempt if it's the same as in the solution ✓
- How many times the same color is in the attempt and compare with the same count in the solution.
- Let the computer generate a set of colors

## How to get there?

- Pyside2? does not seem to work on Android.
- PyQt could work but nobody seems to have managed it.
- Kivy might be the solution.

Next steps:

- [The Kivy Crash Course](https://kivy.org/doc/stable/tutorials/crashcourse.html)
- [Build desktop GUI apps using Python](https://likegeeks.com/kivy-tutorial/) (with a ListView)
- [The kv Design Language](https://www.techwithtim.net/tutorials/kivy-tutorial/the-kv-design-language-kv-file/) (4th part in a series in mobile app develoment with Python)
- probably, use a screen manager: <https://stackoverflow.com/questions/46407668/kivy-widget-in-python-to-kv-file-how-control-it>
- [ListView](https://kivy.org/doc/stable-1.10.1/api-kivy.uix.listview.html)
- If we want random colors, we might also want to ensure that there is enough difference: [How to compare two colors for similarity/difference](https://stackoverflow.com/questions/9018016/how-to-compare-two-colors-for-similarity-difference)

Finding a name:

- Colori
- ColorMind (but "ColorMind!" already exists)
- ColorCode there is an abandoned qt app
- ColorCoder is an Android game

## Kivy

- `pip install kivy`
- create a test app from the Kivy site
- `pip install buildozer`
- `buildozer init`
- edit `buildozer.spec`
  - I only needed to fill the most obvious fields
- `buildozer -v android debug`
- Start the emulator or connect an android device
  - with the usb debugging and the file transfer activated
- `~/Android/Sdk/platform-tools/adb install -r bin/mastermind-0.1-armeabi-v7a-debug.apk`
- Debugging: `~/Android/Sdk/platform-tools/adb logcat`...  
  maybe as `~/Android/Sdk/platform-tools/adb logcat 2>&1 | less` and search for kivy

### Notes

- A good example for using Kivy with `.kv` files: [Kivy widget in python to kv file, how control it](https://stackoverflow.com/questions/46407668/kivy-widget-in-python-to-kv-file-how-control-it)
- https://stackoverflow.com/questions/21547353/handling-app-suspend-resume-on-android-in-kivy

## Game

### Notes

```
[ ] [ ] [ ] [ ]
```

```
[o] [ ] [ ] [ ]

[r] [g] [b] [c] [m] [y] [k]
```

```
[b] [g] [c] [m] [✓] 
```

```
[b] [g] [c] [m] ⸬ 
[ ] [ ] [ ] [ ] 

- Do not generate random colors each time:
  - Generate them on the first start
  - Allow to shuffle them at the start of each game
- Allow to put notes on the original colors (one char or one drawing)
- I've tried to draw the hint dots from python, but they are then shown at lest most of the BoxLayout, not at the button's position. (i might be able to use `to_local`... but the solution in the .kv is good enough for now)

## Notes

- [briefcase](https://doc.qt.io/qtforpython/deployment-briefcase.html) is an alternative for packaging python program but it does not work for android and PySide
- if you don't give any unit, it's in pixels, so you usually want to use dp or sp units when giving absolute values, so it scales better on various dpi you can either use '5dp' or dp(5) for example, though the former is going to be better in the future, at least on desktop (will react to dpi change when you move from one screen to the other for example), for now it's the same.
