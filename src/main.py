from kivy.app import App
from kivy.uix.button import Button

from kivy.graphics import Color, Rectangle, Ellipse

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button

from kivy.storage.jsonstore import JsonStore
from kivy.clock import Clock

import random
import time
from collections import Counter

BLACK = (0.0, 0.0, 0.0, 1.0)
WHITE = (1.0, 1.0, 1.0, 1.0)
TRANSPARENT = (1.0, 1.0, 1.0, 0.0)

class ColorButton(Button):
    def __init__(self, button_i=None, color_i=None, **kwargs):
        super().__init__(**kwargs)
        self.color_i = color_i
        self.button_i = button_i

class ColorLongPressButton(ColorButton):
    """ A button with  a long press """
    def __init__(self, on_release, on_long_press,**kwargs):
        super().__init__(on_press=self.start_pressing, on_release=self.stop_pressing, **kwargs)
        self.action = on_release
        self.long_action = on_long_press
        self.time_press = 0

    def start_pressing(self, _):
        self.time_press = time.time()

    def stop_pressing(self, _):
        if time.time() - self.time_press > 0.5:
            self.long_action(self)
        else:
            self.action(self)


class TextButton(Button):
    pass

class CheckResetButton(TextButton):

    def __init__(self, on_check, on_restart, **kwargs):
        self.is_visible = False # is_visible_check_reset_button
        self.on_check = on_check
        self.on_restart = on_restart
        super().__init__(**kwargs)

    def hide(self):
        self.opacity = 0.0
        self.disabled = True
        self.on_release = None

    def set_check(self):
        self.opacity = 1.0
        self.disabled = False
        self.text = '✓'
        self.on_release = self.on_check

    def set_restart(self):
        self.opacity = 1.0
        self.disabled = False
        self.text = '⟳'
        self.on_release = self.on_restart

class ColorButtonList(BoxLayout):
    pass

class HintButtonDots(Button):
    diameter = 20 # r
    br_padding = 20 # top & left: p
    tl_padding = 60 # bottom & right: r + 2 * r
    color_line = {None: TRANSPARENT, True: BLACK, False: BLACK}
    color_fill = {None: TRANSPARENT, True: BLACK, False: WHITE}
    def __init__(self, hints, **kwargs):
        self.line_color = []
        self.fill_color = []
        for hint in hints:
            self.line_color.append(HintButtonDots.color_line[hint])
            self.fill_color.append(HintButtonDots.color_fill[hint])

        super().__init__(**kwargs)

class MainWindow(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.n_colors = 6
        self.n_guesses = 4

        self.is_playing = False

        store = JsonStore('mastermind.json')
        if store.exists('restore'):
            self.is_playing = True
            restore = store.get('restore')
            self.colors = restore['colors']
            self.solution = restore['solution']
            self.attempts = restore['attempts']
            self.guess = restore['guess']
        else:
            self.colors = self.get_new_colors()
            self.solution = self.create_solution()
            self.attempts = []
            self.guess = [None] * self.n_guesses

        print('solution', self.solution)


        self.guess_buttons_list = []
        self.check_reset_button = None

        self.current_guess = None
        self.guessed_colors = sum(1 for _ in filter(None.__ne__, self.guess))

        self.init_colors_list()
        self.init_guesses_list()
        self.init_attemps_list()
        self.init_guess_colors_list()

    def init_colors_list(self):
        for i, color in enumerate(self.colors):
            button = ColorButton(
                color_i=i,
                background_color=color,
                on_release=self.replace_color)
            self.ids.colors_list.add_widget(button)

    def init_guesses_list(self):
        for i, color in enumerate(self.guess):
            button = ColorLongPressButton(
                button_i=i,
                on_release=self.show_pick_colors,
                on_long_press=self.clear_guess_color)
            if color is not None:
                button.background_color = self.colors[color]
                button.color_i = color
            # the button are inserted in inversed order; we need to keep a separate list
            self.guess_buttons_list.append(button)
            self.ids.guess_list.add_widget(button)
        self.check_reset_button = CheckResetButton(
            self.check_current_guess,
            self.restart_game
        )
        if self.guessed_colors == self.n_guesses:
            self.check_reset_button.set_check()
        self.ids.guess_list.add_widget(self.check_reset_button)

    def init_attemps_list(self):
        for attempt in self.attempts:
            colors_list = self.get_attempt_row(attempt)
            hints = self.get_hints(attempt)
            colors_list.add_widget(HintButtonDots(hints=hints))
            self.ids.attempts_list.add_widget(colors_list)

    def init_guess_colors_list(self):
        for i in range(self.n_colors):
            button = ColorButton(
                color_i=i,
                background_color=self.colors[i],
                on_release=self.pick_color)
            self.ids.guess_colors_list.add_widget(button)

    def store_status(self):
        """ if the game is not over, store the current state. it will be restored in init """
        store = JsonStore('mastermind.json')
        if self.is_playing:
            store.put('restore',
                colors=self.colors,
                solution=self.solution,
                attempts=self.attempts,
                guess=self.guess)
        else:
            if store.exists('restore'):
                store.delete('restore')

    def restart_game(self):
        self.attempts.clear()
        self.ids.attempts_list.clear_widgets()
        self.solution = self.create_solution()
        print(self.solution)
        self.check_reset_button.hide()
        self.is_playing = True

    def show_pick_colors(self, clicked_button):
        self.ids.guess_colors_list.opacity = 1.0
        self.ids.guess_colors_list.disabled = False
        self.current_guess = clicked_button.button_i

    def pick_color(self, target):
        """ hide the list of colors, set the index and the color
        of the buton, show the guess button if all color have been guessed."""
        if not self.is_playing:
            self.is_playing = True
        self.ids.guess_colors_list.opacity = 0.0
        self.ids.guess_colors_list.disabled = True
        self.set_guess_color(self.current_guess, target.color_i)
        self.current_guess = None

    def clear_guess_color(self, button):
        if self.guess[button.button_i] is not None:
            self.guess[button.button_i] = None
            button.color_i = None
            button.background_color = WHITE
            self.guessed_colors -= 1
            self.check_reset_button.hide()

    def set_guess_color(self, button_i, color_i):
        if self.guess[button_i] is None:
            self.guessed_colors += 1
            if self.guessed_colors == self.n_guesses:
                self.check_reset_button.set_check()
        self.guess[button_i] = color_i

        guessed_button = self.guess_buttons_list[button_i]
        guessed_button.background_color = self.colors[color_i]
        guessed_button.color_i = color_i


    def get_random_color(self):
        return (random.randint(0, 10) / 10, random.randint(0, 10) / 10, random.randint(0, 10) / 10, 1.0)


    def get_new_colors(self):
        colors = []
        # TODO: make sure that the new color is different enough to the existing ones)
        return [self.get_random_color() for i in range(self.n_colors)]

    def replace_color(self, event):
        color = self.get_random_color()
        self.colors[event.color_i] = color
        event.background_color = color
        for button in self.ids.guess_colors_list.children:
            if button.color_i == event.color_i:
                button.background_color = color
        for button in self.ids.guess_list.children:
            if isinstance(button, ColorButton) and button.color_i == event.color_i:
                button.background_color = color
        for attempt in self.ids.attempts_list.children:
            for button in attempt.children:
                if isinstance(button, ColorButton) and button.color_i == event.color_i:
                    button.background_color = color

    def create_solution(self):
        return random.choices(range(self.n_colors), k=self.n_guesses)

    def clear_guess_buttons(self):
        for button in self.guess_buttons_list:
            button.background_color = WHITE
            button.color_i = None

    def get_attempt_row(self, guess):
        colors_list = ColorButtonList()
        for i, color_i in enumerate(guess):
            colors_list.add_widget(ColorButton(
                color_i=color_i,
                background_color=self.colors[color_i],
                button_i=i,
                on_release=self.attempt_clicked
            ))
        return colors_list

    def check_current_guess(self):

        colors_list = self.get_attempt_row(self.guess)

        hints = self.get_hints(self.guess)
        colors_list.add_widget(HintButtonDots(hints=hints))
        self.attempts.append(self.guess)
        self.ids.attempts_list.add_widget(colors_list)

        self.guessed_colors = 0
        self.guess = [None] * self.n_guesses

        if all(hints):
            self.is_playing = False
            self.clear_guess_buttons()
            self.check_reset_button.set_restart()
        else:
            self.clear_guess_buttons()
            self.check_reset_button.hide()

    def attempt_clicked(self, event):
        self.set_guess_color(event.button_i, event.color_i)

    def get_hints(self, guess):
        correct, incorrect = self.get_correct_and_incorrect(guess, self.solution)

        return [True] * correct + [False] * incorrect + [None] * (self.n_guesses - correct - incorrect)

    def get_correct_and_incorrect(self, attempt, solution):
        correct = 0
        incorrect_attempt = []
        incorrect_solution = []
        for a, s in zip(attempt, solution):
            if a == s:
                correct += 1
            else:
                incorrect_attempt.append(a)
                incorrect_solution.append(s)
        incorrect = 0
        attempt_counter = Counter(incorrect_attempt)
        solution_counter = Counter(incorrect_solution)
        for k, v in attempt_counter.items():
            if k in solution_counter:
                incorrect += min(v, solution_counter[k])
        return correct, incorrect


class MastermindApp(App):
    title = "Mastermind"

    def on_pause(self):
        self.mainWindow.store_status()
        return true

    def on_stop(self):
        self.mainWindow.store_status()

    def build(self):
        self.mainWindow = MainWindow()
        return self.mainWindow

if __name__ == "__main__":
    MastermindApp().run()
